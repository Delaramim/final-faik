const API_ENDPOINT = `http://localhost:3000`;

const getAll = async () => {
  const response = await fetch(API_ENDPOINT);
  const json = await response.json();
  return json.data.people;
};

const getById = async (id) => {
  const response = await fetch(`${API_ENDPOINT}/${id}`);
  const json = await response.json();
  return json.data.person;
};

const addToInvited = async (id) => {
  const response = await fetch(`${API_ENDPOINT}/invited`, {
    method: "POST",
    body: JSON.stringify({
      invited: id,
    }),
  });
  const json = await response.json();
  return json.data;
};

export { getAll, getById, addToInvited };
