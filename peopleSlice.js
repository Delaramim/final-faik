import { createSlice, configureStore } from "@reduxjs/toolkit";
import data from "./data.json";

const peopleSlice = createSlice({
  name: "people",
  initialState: {
    values: [],
    invited: [],
  },
  reducers: {
    addPeople: (state, action) => {
      state.values = action.payload;
    },
    addPerson: (state, action) => {
      state.values.push(action.payload);
    },
    addToInvited: (state, action) => {
      console.log("ADDED", action.payload);
      state.invited.push(action.payload);
    },
    notifyInvited: (state, action) => {
      state.invited = [];
    },
    removeFromInvited: (state, action) => {
      console.log("Removes", action.payload);
      state.invited = state.invited.filter((x) => x.id !== action.payload);
    },
  },
});

export const {
  addPeople,
  addPerson,
  addToInvited,
  notifyInvited,
  removeFromInvited,
} = peopleSlice.actions;

export const store = configureStore({
  reducer: {
    people: peopleSlice.reducer,
  },
});
