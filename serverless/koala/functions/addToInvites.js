"use strict";
const http = require("../utils/http");
const data = require("../data");

module.exports.handler = async (event) => {
  try {
    const { invited } = JSON.parse(event.body);
    if (!invtied) return http.unprocessableEntity();

    // const person = data.find((x) => x.id === parseInt(personID));
    // if (!person) return http.notFound("Episode not found.");

    return http.ok({ invited: invited });
  } catch (error) {
    console.error(error);
    return http.error();
  }
};
