"use strict";
const http = require("../utils/http");
const data = require("../data");

module.exports.handler = async (event) => {
  try {
    const { id } = event.pathParameters;
    if (!id) return http.unprocessableEntity();

    const person = data.find((x) => x.id == id);
    if (!person) return http.notFound("Person not found.");

    return http.ok({ person });
  } catch (error) {
    return http.error();
  }
};
