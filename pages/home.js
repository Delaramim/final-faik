import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { getAll } from "../api";
import { useDispatch, useSelector } from "react-redux";
import { addPeople } from "../peopleSlice";
const Home = ({ navigation }) => {
  const dispatch = useDispatch();
  const people = useSelector((state) => state.people.values);

  const fetchData = async () => {
    const newPeople = await getAll();

    dispatch(addPeople(newPeople));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <ScrollView style={styles.container}>
      {people?.map((person, index) => {
        return (
          <TouchableOpacity
            key={index}
            style={{ padding: 20 }}
            onPress={() => navigation.navigate("Details", { id: person.id })}
          >
            <View style={styles.row}>
              <Image
                style={styles.image}
                source={{
                  uri: "https://reactnative.dev/img/tiny_logo.png",
                }}
              />
              <View style={{ marginLeft: 20 }}>
                <Text>{person.name}</Text>
                <Text>{person.age}</Text>
                <Text>{person.email}</Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  image: {
    width: 40,
    height: 40,
  },
  container: { padding: 5 },
  listContainer: {
    borderBottomWidth: 2,
    borderColor: "pink",
    padding: 20,
    flexDirection: "row",
  },
  row: {
    flexDirection: "row",
  },
});
