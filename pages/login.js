import React, { useState, useEffect } from "react";
import { View, Text, TextInput, StyleSheet, Button } from "react-native";
import { addToInvited } from "../api";
import { useSelector } from "react-redux";
import { getAll } from "../api";

const Login = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [people, setPeople] = useState(null);

  const fetchData = async () => {
    const people = await getAll();
    setPeople(people);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const valid = people?.find((x) => x.email === email);
  const handleSubmit = () => {
    // console.log("Login" + email + " " + password);
    navigation.navigate("Home");
  };
  return (
    <View>
      <TextInput
        style={styles.input}
        value={email}
        onChangeText={setEmail}
        autoCapitalize="none"
        autoCorrect={false}
      />
      <TextInput
        style={styles.input}
        value={password}
        onChangeText={setPassword}
      />
      <Button title="Submit" onPress={() => handleSubmit()}></Button>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  input: {
    width: "90%",
    height: 50,
    borderColor: "pink",
    borderWidth: 3,
    margin: 20,
  },
});
