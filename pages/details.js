import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, Image, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { getById } from "../api";
import { addToInvited, removeFromInvited } from "../peopleSlice";

const Details = ({ route }) => {
  const { id } = route.params;
  const dispatch = useDispatch();

  const person = useSelector((state) =>
    state.people.values.find((i) => i.id === id)
  );

  const invited = useSelector((state) =>
    state.people.invited.find((i) => i.id === id)
  );
  console.log("Invited", invited);
  // const fetchData = async () => {

  //   const newPerson = await getById(id);
  //   dispatch(addPerson(newPerson));
  // };

  // useEffect(() => {
  //   if (!person ) fetchData();
  // }, [person]);

  const handleInvite = () => {
    console.log(person);
    dispatch(addToInvited(person));
  };
  const handleUninvite = () => {
    dispatch(removeFromInvited(person.id));
  };
  return (
    <View style={{ padding: 20, alignContent: "center", alignItems: "center" }}>
      <Image
        style={styles.image}
        source={{
          uri: "https://reactnative.dev/img/tiny_logo.png",
        }}
      />
      <View style={{ marginLeft: 20 }}>
        <Text>{person.name}</Text>
        <Text>{person.email}</Text>
        <Text>{person.description}</Text>
      </View>
      {!invited ? (
        <Button title="Invite" onPress={() => handleInvite()} />
      ) : (
        <Button title="Uninvite" onPress={() => handleUninvite()} />
      )}
    </View>
  );
};

export default Details;

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200,
  },
  container: { padding: 5, alignSelf: "center" },
  listContainer: {
    borderBottomWidth: 2,
    borderColor: "pink",
    padding: 20,
    flexDirection: "row",
  },
  row: {
    flexDirection: "row",
  },
});
