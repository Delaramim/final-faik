import Home from "./home";
import Login from "./login";
import Details from "./details";
import Invited from "./invited";

export { Home, Login, Details, Invited };
