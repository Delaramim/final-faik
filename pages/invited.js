import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Button,
} from "react-native";

import { useDispatch, useSelector } from "react-redux";
import { notifyInvited } from "../peopleSlice";
import { addInvited } from "../api";

const Invited = ({ navigation }) => {
  const dispatch = useDispatch();
  const invited = useSelector((state) => state.people.invited);
  const handleInvite = () => {
    addInvited(invited);
    dispatch(notifyInvited());
  };

  return (
    <ScrollView style={styles.container}>
      <Button title={"Notify Invites"} onPress={() => handleInvite()} />
      {invited?.map((person, index) => {
        return (
          <TouchableOpacity
            key={index}
            style={{ padding: 20 }}
            onPress={() => navigation.navigate("Details", { id: person.id })}
          >
            <View style={styles.row}>
              <Image
                style={styles.image}
                source={{
                  uri: "https://reactnative.dev/img/tiny_logo.png",
                }}
              />
              <View style={{ marginLeft: 20 }}>
                <Text>{person.name}</Text>
                <Text>{person.age}</Text>
                <Text>{person.email}</Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

export default Invited;

const styles = StyleSheet.create({
  image: {
    width: 40,
    height: 40,
  },
  container: { padding: 5 },
  listContainer: {
    borderBottomWidth: 2,
    borderColor: "pink",
    padding: 20,
    flexDirection: "row",
  },
  row: {
    flexDirection: "row",
  },
});
